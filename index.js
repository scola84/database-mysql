'use strict';

const mysql = require('mysql');

const Async = require('@scola/async');
const Deep = require('@scola/deep');
const DI = require('@scola/di');

const Database = require('./lib/database.js');

class Module extends DI.Module {
  configure() {
    this.inject(Database).with(
      this.object({}),
      this.provider(Async.Serial),
      this.provider(Async.Parallel),
      this.instance(Deep.Map),
      this.value(mysql)
    );
  }
}

module.exports = {
  Database,
  Module
};
