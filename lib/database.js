'use strict';

const Error = require('@scola/error');
const EventHandler = require('@scola/events');

class Database extends EventHandler {
  constructor(queries, serial, parallel, options, mysql) {
    super();

    this.queries = queries;
    this.mysql = mysql;

    this.serialRead = serial.get()
      .setProcessor(this.processSerialRead.bind(this));

    this.parallelRead = parallel.get()
      .setProcessor(this.processParallelRead.bind(this));

    this.serialWrite = serial.get()
      .setProcessor(this.processSerialWrite.bind(this));

    this.parallelWrite = parallel.get()
      .setProcessor(this.processParallelWrite.bind(this));

    this.options = options.assign({
      queryFormat: this.queryFormat
    });
  }

  getOptions() {
    return this.options;
  }

  connect(options) {
    this.emit('debug', this, 'connect', options);

    this.options.assign(options);
    this.pool = this.mysql.createPool(this.options.toJSON());

    return this;
  }

  read(name, parameters) {
    this.emit('debug', this, 'read', name, parameters);
    return this.execute(name, this.handleRead, parameters);
  }

  readAll(name, parameters) {
    this.emit('debug', this, 'readAll', name, parameters);
    return this.execute(name, this.handleReadAll, parameters);
  }

  readSerial(queries) {
    this.emit('debug', this, 'readSerial', queries);
    return this.serialRead.process(queries);
  }

  readParallel(queries) {
    this.emit('debug', this, 'readParallel', queries);
    return this.parallelRead.process(queries);
  }

  write(name, parameters) {
    this.emit('debug', this, 'write', name, parameters);
    return this.execute(name, this.handleWrite, parameters);
  }

  writeSerial(queries) {
    this.emit('debug', this, 'writeSerial', queries);
    return this.serialWrite.process(queries);
  }

  writeParallel(queries) {
    this.emit('debug', this, 'writeParallel', queries);
    return this.parallelWrite.process(queries);
  }

  execute(name, handler, ...parameters) {
    this.emit('debug', this, 'execute', name, parameters);

    return new Promise((resolve, reject) => {
      if (!this.queries[name]) {
        return reject(new Error('database_query_not_found', {
          detail: {
            name
          }
        }));
      }

      return this.pool.getConnection((error, connection) => {
        if (error) {
          return reject(new Error('database_not_connected', {
            origin: error,
            detail: {
              options: this.options
            }
          }));
        }

        return this.queries[name](
          connection,
          handler.bind(this, connection, name, resolve, reject),
          ...parameters
        );
      });
    });
  }

  processSerialRead(definition, parameters, results) {
    this.emit('debug', this, 'processSerialRead',
      definition, parameters, results);

    return this.execute(
      definition.queryAll ? definition.queryAll : definition.query,
      definition.queryAll ? this.handleReadAll : this.handleRead,
      definition.parameters,
      results
    );
  }

  processParallelRead(definition) {
    this.emit('debug', this, 'processParallelRead', definition);

    return this.execute(
      definition.queryAll ? definition.queryAll : definition.query,
      definition.queryAll ? this.handleReadAll : this.handleRead,
      definition.parameters
    );
  }

  handleRead(connection, name, resolve, reject, error, result) {
    this.emit('debug', this, 'handleRead', name, error, result);

    connection.release();

    if (error) {
      return reject(new Error('database_read_not_executed', {
        origin: error,
        detail: {
          name
        }
      }));
    }

    return resolve(result[0]);
  }

  handleReadAll(connection, name, resolve, reject, error, result) {
    this.emit('debug', this, 'handleReadAll', name, error, result);

    connection.release();

    if (error) {
      return reject(new Error('database_read_not_executed', {
        origin: error,
        detail: {
          name
        }
      }));
    }

    return resolve(result);
  }

  processSerialWrite(definition, parameters, results) {
    this.emit('debug', this, 'processSerialWrite',
      definition, parameters, results);

    return this.execute(
      definition.query,
      this.handleWrite,
      definition.parameters,
      results
    );
  }

  processParallelWrite(definition) {
    this.emit('debug', this, 'processParallelWrite', definition);

    return this.execute(
      definition.query,
      this.handleWrite,
      definition.parameters
    );
  }

  handleWrite(connection, name, resolve, reject, error, result) {
    this.emit('debug', this, 'handleWrite', name, error, result);

    connection.release();

    if (error) {
      return reject(new Error('database_write_not_executed', {
        origin: error,
        detail: {
          name
        }
      }));
    }

    if (result.insertId) {
      return resolve({
        id: result.insertId
      });
    }

    return resolve({
      changed: result.changedRows,
      unchanged: result.affectedRows - result.changedRows
    });
  }

  queryFormat(query, parameters) {
    if (!parameters) {
      return query;
    }

    return query.replace(/\:(\w+)/g, (text, key) => {
      if (parameters.hasOwnProperty(key)) {
        return this.escape(parameters[key]);
      }

      return text;
    });
  }
}

module.exports = Database;
